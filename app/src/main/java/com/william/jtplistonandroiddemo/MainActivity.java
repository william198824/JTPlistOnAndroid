package com.william.jtplistonandroiddemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.william.jtplistonandroidt.JTPlistOnAndroidT;

import java.io.InputStream;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InputStream is = getResources().openRawResource(R.raw.pagesconfig);
        //这里的泛型用来确定返回值的类型.
        //如果plist文件的根节点是<dict>,这个泛型就写HashMap,如果根节点是Array,则这个泛型应该为ArrayList.
        //read()方法会解析plist文件,如果plist文件的根节点是<dict>则本方法返回HashMap;如果根节点未<Array>,则本方法返回一个ArrayList.
        //read()执行完可自动关闭InputStream,不需要各位自己写关闭了,省的大家又是关闭又得嵌套try-catch.
        HashMap plist = new JTPlistOnAndroidT<HashMap>().read(is);
        Log.i("", "onCreate: 请断点调试Plist变量的值!");
    }
}
